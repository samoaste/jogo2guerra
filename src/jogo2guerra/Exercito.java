package jogo2guerra;

public enum Exercito {

    EUA("EUA Bandeira: vermelha"),
    INGLATERRA("INGLATERRA Bandeira: branca e vermelha"),
    FRANÇA("FRANÇA Bandeira: azul"),
    JAPAO("JAPÃO Bandeira: preta"),
    ALEMANHA("ALEMANHA Bandeira: amarela"),
    ITALIA("ITALIA Bandeira: verde"),
    ALVO("Alvo");
    
    private final String descricao;

    private Exercito(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
