package jogo2guerra;

abstract class Personagem implements Movimentavel {

    private Exercito descricao;
    private int linha;
    private int coluna;
    private String coordenada;
    private int movimentacao;

    public Personagem() {        
    }

    public int getLinha() {
        return linha;
    }

    public void setLinha(int linha) {
        this.linha = linha;
    }

    public int getColuna() {
        return coluna;
    }

    public void setColuna(int coluna) {
        this.coluna = coluna;
    }

    public String getNome() {
        return descricao.getDescricao();
    }

    public void setNome(Exercito descricao) {
        this.descricao = descricao;
    }
    
    public String getCoordenada() {
        return coordenada;
    }

    public void setCoordenada(String coordenada) {
        this.coordenada = coordenada;
    }

    public int getMovimentacao() {
        return movimentacao;
    }

    public void setMovimentacao(int movimentacao) {
        this.movimentacao = movimentacao;
    }
    
    public void movimentar() {
        int linha = this.getLinha();
        linha += this.getMovimentacao();
        this.setLinha(linha);

        int coluna = this.getColuna();
        coluna += this.getMovimentacao();
        this.setColuna(coluna);
        
        this.setCoordenada(this.getLinha() + ", " + this.getColuna());
        
    }
    
}
