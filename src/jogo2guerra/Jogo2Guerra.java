package jogo2guerra;

public class Jogo2Guerra {

    public static void main(String[] args) {
        
        Controle controle = new Controle();
        controle.setRodada(5);
        
        Alvo alvo = new Alvo();
        Amigo amigoEUA = new Amigo();
        Inimigo inimigoJap = new Inimigo();

        alvo.setNome(Exercito.ALVO);
        amigoEUA.setNome(Exercito.EUA);
        inimigoJap.setNome(Exercito.JAPAO);
        
        /* TESTE 1 - AMIGO VENCE */
        alvo.setColuna(3);
        alvo.setLinha(3);
        alvo.setMovimentacao(1);

        amigoEUA.setColuna(1);
        amigoEUA.setLinha(1);
        amigoEUA.setMovimentacao(2);

        inimigoJap.setColuna(4);
        inimigoJap.setLinha(1);
        inimigoJap.setMovimentacao(3);
        
        /*TESTE 2 - INIMIGO VENCE
         alvo.setColuna(3);
         alvo.setLinha(3);
         alvo.setMovimentacao(1);
        
         amigoEUA.setColuna(4);
         amigoEUA.setLinha(1);
         amigoEUA.setMovimentacao(3);
        
         inimigoJap.setColuna(1);
         inimigoJap.setLinha(1);
         inimigoJap.setMovimentacao(2);
        */ 
        
        /* TESTE 3 - ALVO A DERIVA PARA SEMPRE 
         alvo.setColuna(3);
         alvo.setLinha(3);
         alvo.setMovimentacao(10);
        
         amigoEUA.setColuna(1);
         amigoEUA.setLinha(1);
         amigoEUA.setMovimentacao(2);
        
         inimigoJap.setColuna(4);
         inimigoJap.setLinha(1);
         inimigoJap.setMovimentacao(3);
        */
        
        System.out.println("Iniciando Jogo... Posições iniciais:");
        System.out.println("Personagem (" + alvo.getLinha() + ", " + alvo.getColuna() + ") -- " + alvo.getNome());
        System.out.println("Personagem (" + amigoEUA.getLinha() + ", " + amigoEUA.getColuna() + ") -- " + amigoEUA.getNome());
        System.out.println("Personagem (" + inimigoJap.getLinha() + ", " + inimigoJap.getColuna() + ") -- " + inimigoJap.getNome());

        for (int rodada = 1; rodada <= controle.getRodada(); rodada++) {

            alvo.movimentar();
            amigoEUA.movimentar();
            inimigoJap.movimentar();
            
            System.out.println("Rodada: " + rodada);
            System.out.println("Personagem (" + alvo.getCoordenada() + ") -- " + alvo.getNome());
            System.out.println("Personagem (" + amigoEUA.getCoordenada() + ") -- " + amigoEUA.getNome());
            System.out.println("Personagem (" + inimigoJap.getCoordenada() + ") -- " + inimigoJap.getNome());

            if (amigoEUA.getCoordenada().equals(alvo.getCoordenada())) {
                System.out.println("Game over: Alvo encontrado por " + amigoEUA.getNome());
                System.exit(rodada);

            } else if (inimigoJap.getCoordenada().equals(alvo.getCoordenada())) {
                System.out.println("Game over: Alvo estuprado por " + inimigoJap.getNome());
                System.exit(rodada);
            }

        }
        
        System.out.println("Alvo a deriva no mar para sempre!");

    }

}
